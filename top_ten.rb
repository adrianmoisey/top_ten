number_of_results = 10

top_n = []
f = File.open('big_file.txt', 'r')
minimum = 0

f.each_line do |line|
  next unless line.to_i > minimum

  top_n.push(line.to_i)
  minimum = top_n.min
  top_n.delete(minimum) if top_n.length > number_of_results
end

f.close

puts top_n.sort.reverse
