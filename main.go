package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func Min(array []int) (int, int) {
	var min = array[0]
	var index = 0
	for i, value := range array {
		if min > value {
			min = value
			index = i
		}
	}
	return min, index
}

func main() {
	file, err := os.Open("big_file.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	results := 10
	var topN []int
	var minimum int
	var index int

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		number, _ := strconv.Atoi(line)
		if number > minimum {
			topN = append(topN, number)
			minimum, index = Min(topN)
			if len(topN) > results {
				topN[index] = topN[len(topN)-1]
				topN[len(topN)-1] = 0
				topN = topN[:len(topN)-1]
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	sort.Ints(topN)
	for _, n := range topN {
		fmt.Println(n)
	}
}
